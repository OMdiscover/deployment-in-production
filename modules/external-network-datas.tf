###
# Get datas from network "external"
###

data "openstack_networking_network_v2" "ext_network" {
  name         = var.external_network
}

###
# Get datas from secret key
###
